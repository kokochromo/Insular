- [ ] I'm aware that the following types bug reports won't be processed:
    - bugs that should be fixed by the upstream, e.g. feature related (should be sent to the upstream directly: https://github.com/oasisfeng/island/issues/new/choose)
    - bugs that can't be reproduced

## Summary

(Summarize the bug encountered concisely)

## Steps to reproduce

(How one can reproduce the issue - this is very important)

## What is the current bug behavior?

(What actually happens)

## What is the expected correct behavior?

(What you should see instead)

## Relevant logs and/or screenshots

(Paste any relevant logs - please use code blocks (```) to format console output, logs, and code, as
it's very hard to read otherwise.)

## Possible fixes

(If you can, link to the line of code that might be responsible for the problem)

/label ~bug ~needs-investigation